/*! Unit tests that require access to the testing-only function
BridgeLine::random() or private fields */

use super::bridge_table::BridgeLine;
use super::proto::*;
use super::*;
use chrono::{DateTime, NaiveTime, Timelike, Utc};
use rand::Rng;
use statistical::{mean, standard_deviation};
use std::collections::HashSet;
use std::thread;
use std::time::{Duration, Instant};

#[cfg(feature = "fast")]
const USERS: usize = 100;
#[cfg(not(feature = "fast"))]
const USERS: usize = 10000;

struct PerfStat {
    // Report performance metrics for each test
    req_len: usize,
    resp_len: usize,
    req_t: Duration,
    resp_t: Duration,
    resp_handle_t: Duration,
}

struct TestHarness {
    bdb: BridgeDb,
    pub ba: BridgeAuth,
}

impl TestHarness {
    fn new() -> Self {
        TestHarness::new_buckets(5, 5)
    }

    fn new_buckets(num_buckets: u16, hot_spare: u16) -> Self {
        // Create a BridegDb
        let mut bdb = BridgeDb::new();
        // Create a BridgeAuth
        let mut ba = BridgeAuth::new(bdb.pubkey);

        // Make 3 x num_buckets open invitation bridges, in sets of 3
        for _ in 0..num_buckets {
            let bucket = [
                BridgeLine::random(),
                BridgeLine::random(),
                BridgeLine::random(),
            ];
            ba.add_openinv_bridges(bucket, &mut bdb);
        }
        // Add hot_spare more hot spare buckets
        for _ in 0..hot_spare {
            let bucket = [
                BridgeLine::random(),
                BridgeLine::random(),
                BridgeLine::random(),
            ];
            ba.add_spare_bucket(bucket);
        }
        // Create the encrypted bridge table
        ba.enc_bridge_table();

        Self { bdb, ba }
    }

    fn advance_days(&mut self, days: u16) {
        self.ba.advance_days(days);
    }

    fn open_invite(&mut self) -> (PerfStat, (cred::Lox, bridge_table::BridgeLine)) {
        // Issue an open invitation
        let inv = self.bdb.invite();

        let req_start = Instant::now();
        // Use it to get a Lox credential
        let (req, state) = open_invite::request(&inv);
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_open_invite(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let (cred, bridgeline) =
            open_invite::handle_response(state, decode_resp, &self.ba.lox_pub).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            (cred, bridgeline),
        )
    }

    fn trust_promotion(&mut self, cred: &cred::Lox) -> (PerfStat, cred::Migration) {
        let req_start = Instant::now();
        let (promreq, promstate) =
            trust_promotion::request(cred, &self.ba.lox_pub, self.ba.today()).unwrap();
        let encoded: Vec<u8> = bincode::serialize(&promreq).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let promresp = self.ba.handle_trust_promotion(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&promresp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let migcred = trust_promotion::handle_response(promstate, decode_resp).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            migcred,
        )
    }

    fn level0_migration(
        &mut self,
        loxcred: &cred::Lox,
        migcred: &cred::Migration,
    ) -> (PerfStat, cred::Lox) {
        let req_start = Instant::now();
        let (migreq, migstate) =
            migration::request(loxcred, migcred, &self.ba.lox_pub, &self.ba.migration_pub).unwrap();
        let encoded: Vec<u8> = bincode::serialize(&migreq).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let migresp = self.ba.handle_migration(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&migresp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp: migration::Response = bincode::deserialize(&encoded_resp[..]).unwrap();
        let cred = migration::handle_response(migstate, decode_resp, &self.ba.lox_pub).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            cred,
        )
    }

    fn level_up(&mut self, cred: &cred::Lox) -> (PerfStat, cred::Lox) {
        // Read the bucket in the credential to get today's Bucket
        // Reachability credential

        let (id, key) = bridge_table::from_scalar(cred.bucket).unwrap();
        let encbuckets = self.ba.enc_bridge_table();
        let bucket =
            bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
        let reachcred = bucket.1.unwrap();

        // Use the Bucket Reachability credential to advance to the next
        // level
        let req_start = Instant::now();
        let (req, state) = level_up::request(
            cred,
            &reachcred,
            &self.ba.lox_pub,
            &self.ba.reachability_pub,
            self.ba.today(),
        )
        .unwrap();
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_level_up(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let cred = level_up::handle_response(state, decode_resp, &self.ba.lox_pub).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            cred,
        )
    }

    fn issue_invite(&mut self, cred: &cred::Lox) -> (PerfStat, (cred::Lox, cred::Invitation)) {
        // Read the bucket in the credential to get today's Bucket
        // Reachability credential
        let (id, key) = bridge_table::from_scalar(cred.bucket).unwrap();
        let encbuckets = self.ba.enc_bridge_table();
        let bucket =
            bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
        let reachcred = bucket.1.unwrap();

        let req_start = Instant::now();
        let (req, state) = issue_invite::request(
            cred,
            &reachcred,
            &self.ba.lox_pub,
            &self.ba.reachability_pub,
            self.ba.today(),
        )
        .unwrap();
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_issue_invite(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let (cred, invite) = issue_invite::handle_response(
            state,
            decode_resp,
            &self.ba.lox_pub,
            &self.ba.invitation_pub,
        )
        .unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            (cred, invite),
        )
    }

    fn redeem_invite(&mut self, inv: &cred::Invitation) -> (PerfStat, cred::Lox) {
        let req_start = Instant::now();
        let (req, state) =
            redeem_invite::request(inv, &self.ba.invitation_pub, self.ba.today()).unwrap();
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_redeem_invite(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let cred = redeem_invite::handle_response(state, decode_resp, &self.ba.lox_pub).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            cred,
        )
    }

    fn check_blockage(&mut self, cred: &cred::Lox) -> (PerfStat, cred::Migration) {
        let req_start = Instant::now();
        let (req, state) = check_blockage::request(cred, &self.ba.lox_pub).unwrap();
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_check_blockage(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp = bincode::deserialize(&encoded_resp[..]).unwrap();
        let migcred = check_blockage::handle_response(state, decode_resp).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            migcred,
        )
    }

    fn blockage_migration(
        &mut self,
        cred: &cred::Lox,
        mig: &cred::Migration,
    ) -> (PerfStat, cred::Lox) {
        let req_start = Instant::now();
        let (req, state) =
            blockage_migration::request(cred, mig, &self.ba.lox_pub, &self.ba.migration_pub)
                .unwrap();
        let encoded: Vec<u8> = bincode::serialize(&req).unwrap();
        let req_t = req_start.elapsed();
        let req_len = encoded.len();

        let resp_start = Instant::now();
        let decoded = bincode::deserialize(&encoded[..]).unwrap();
        let resp = self.ba.handle_blockage_migration(decoded).unwrap();
        let encoded_resp: Vec<u8> = bincode::serialize(&resp).unwrap();
        let resp_t = resp_start.elapsed();
        let resp_len = encoded_resp.len();

        let resp_handle_start = Instant::now();
        let decode_resp: blockage_migration::Response =
            bincode::deserialize(&encoded_resp[..]).unwrap();
        let cred =
            blockage_migration::handle_response(state, decode_resp, &self.ba.lox_pub).unwrap();
        let resp_handle_t = resp_handle_start.elapsed();

        (
            PerfStat {
                req_len,
                resp_len,
                req_t,
                resp_t,
                resp_handle_t,
            },
            cred,
        )
    }
}

#[test]
fn test_open_invite() {
    let mut th = TestHarness::new();

    // Join an untrusted user
    let (perf_stat, (cred, bridgeline)) = th.open_invite();

    // Check that we can use the credential to read a bucket
    let (id, key) = bridge_table::from_scalar(cred.bucket).unwrap();
    let encbuckets = th.ba.enc_bridge_table();
    let bucket =
        bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
    print_test_results(perf_stat);
    println!("cred = {:?}", cred);
    println!("bucket = {:?}", bucket);
    println!("bridgeline = {:?}", bridgeline);
    assert!(bucket.1.is_none());
    assert!(th.ba.verify_lox(&cred));
    assert!(bridgeline == bucket.0[0]);
}

#[test]
fn test_trust_promotion() {
    let mut th = TestHarness::new();

    let cred = th.open_invite().1 .0;
    assert!(th.ba.verify_lox(&cred));

    // Time passes
    th.advance_days(47);

    let (perf_stat, migcred) = th.trust_promotion(&cred);
    assert!(th.ba.verify_migration(&migcred));

    // Check that we can use the to_bucket in the Migration credenital
    // to read a bucket
    let (id, key) = bridge_table::from_scalar(migcred.to_bucket).unwrap();
    let encbuckets = th.ba.enc_bridge_table();
    let bucket =
        bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
    print_test_results(perf_stat);
    println!("bucket = {:?}", bucket);
    assert!(th.ba.verify_reachability(&bucket.1.unwrap()));
}

#[test]
fn test_level0_migration() {
    let mut th = TestHarness::new();

    let cred = th.open_invite().1 .0;
    assert!(th.ba.verify_lox(&cred));

    // Time passes
    th.advance_days(47);

    let (perf_stat, migcred) = th.trust_promotion(&cred);
    assert!(th.ba.verify_migration(&migcred));
    println!("--Trust Promotion to 1--\n");
    print_test_results(perf_stat);

    let (mperf_stat, newloxcred) = th.level0_migration(&cred, &migcred);

    println!("--Level 0 migration--\n");
    print_test_results(mperf_stat);

    assert!(th.ba.verify_lox(&newloxcred));
    println!("newloxcred = {:?}", newloxcred);
    // Check that we can use the credenital to read a bucket
    let (id, key) = bridge_table::from_scalar(newloxcred.bucket).unwrap();
    let encbuckets = th.ba.enc_bridge_table();
    let bucket =
        bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
    println!("bucket = {:?}", bucket);
    assert!(th.ba.verify_reachability(&bucket.1.unwrap()));
}

#[test]
fn test_level_up() {
    let mut th = TestHarness::new();

    // Join an untrusted user
    let cred = th.open_invite().1 .0;

    // Time passes
    th.advance_days(47);

    // Go up to level 1
    let (perf_stat, migcred) = th.trust_promotion(&cred);

    println!("--Trust Promotion to 1--\n");
    print_test_results(perf_stat);

    let (mperf_stat, cred1) = th.level0_migration(&cred, &migcred);

    println!("--New Level 1 Credential--\n");
    print_test_results(mperf_stat);

    assert!(scalar_u32(&cred1.trust_level).unwrap() == 1);

    // Time passes
    th.advance_days(20);

    let (two_perf_stat, cred2) = th.level_up(&cred1);
    assert!(scalar_u32(&cred2.trust_level).unwrap() == 2);

    println!("--Upgrade to Level 2--\n");
    print_test_results(two_perf_stat);
    println!("cred2 = {:?}", cred2);
    assert!(th.ba.verify_lox(&cred2));

    // Time passes
    th.advance_days(30);

    let (three_perf_stat, cred3) = th.level_up(&cred2);
    assert!(scalar_u32(&cred3.trust_level).unwrap() == 3);
    println!("--Upgrade to Level 3--\n");
    print_test_results(three_perf_stat);
    println!("cred3 = {:?}", cred3);
    assert!(th.ba.verify_lox(&cred3));

    // Time passes
    th.advance_days(60);

    let (four_perf_stat, cred4) = th.level_up(&cred3);
    assert!(scalar_u32(&cred3.trust_level).unwrap() == 3);
    println!("--Upgrade to Level 4--\n");
    print_test_results(four_perf_stat);
    println!("cred4 = {:?}", cred4);
    assert!(th.ba.verify_lox(&cred4));
}

#[test]
fn test_issue_invite() {
    let mut th = TestHarness::new();

    // Join an untrusted user
    let cred = th.open_invite().1 .0;

    // Time passes
    th.advance_days(47);

    // Go up to level 1
    let (perf_stat, migcred) = th.trust_promotion(&cred);
    println!("--Trust Promotion to 1--\n");
    print_test_results(perf_stat);
    let (mperf_stat, cred1) = th.level0_migration(&cred, &migcred);
    println!("--New Level 1 Credential--\n");
    print_test_results(mperf_stat);
    assert!(scalar_u32(&cred1.trust_level).unwrap() == 1);

    // Time passes
    th.advance_days(20);

    // Go up to level 2
    let (two_perf_stat, cred2) = th.level_up(&cred1);
    println!("--Upgrade to Level 2--\n");
    print_test_results(two_perf_stat);
    assert!(scalar_u32(&cred2.trust_level).unwrap() == 2);
    println!("cred2 = {:?}", cred2);
    assert!(th.ba.verify_lox(&cred2));

    // Issue an invitation
    let (invite_perf_stat, (cred2a, invite)) = th.issue_invite(&cred2);
    println!("--Issue Invitation--\n");
    print_test_results(invite_perf_stat);
    assert!(th.ba.verify_lox(&cred2a));
    assert!(th.ba.verify_invitation(&invite));
    println!("cred2a = {:?}", cred2a);
    println!("invite = {:?}", invite);
}

#[test]
fn test_redeem_invite() {
    let mut th = TestHarness::new();

    // Join an untrusted user
    let cred = th.open_invite().1 .0;

    // Time passes
    th.advance_days(47);

    // Go up to level 1
    let (perf_stat, migcred) = th.trust_promotion(&cred);
    println!("--Trust Promotion to 1--\n");
    print_test_results(perf_stat);
    let (mperf_stat, cred1) = th.level0_migration(&cred, &migcred);
    println!("--New Level 1 Credential--\n");
    print_test_results(mperf_stat);
    assert!(scalar_u32(&cred1.trust_level).unwrap() == 1);

    // Time passes
    th.advance_days(20);

    // Go up to level 2
    let (two_perf_stat, cred2) = th.level_up(&cred1);
    println!("--Upgrade to Level 2--\n");
    print_test_results(two_perf_stat);
    assert!(scalar_u32(&cred2.trust_level).unwrap() == 2);
    println!("cred2 = {:?}", cred2);
    assert!(th.ba.verify_lox(&cred2));

    // Issue an invitation to Bob
    let (invite_perf_stat, (cred2a, bob_invite)) = th.issue_invite(&cred2);
    println!("--Issue Invitation--\n");
    print_test_results(invite_perf_stat);
    assert!(th.ba.verify_lox(&cred2a));
    assert!(th.ba.verify_invitation(&bob_invite));
    println!("cred2a = {:?}", cred2a);
    println!("bob_invite = {:?}", bob_invite);

    // Time passes
    th.advance_days(12);

    // Bob joins the system
    let (bob_perf_stat, bob_cred) = th.redeem_invite(&bob_invite);
    println!("--Bob joins the system--\n");
    print_test_results(bob_perf_stat);
    assert!(th.ba.verify_lox(&bob_cred));
    println!("bob_cred = {:?}", bob_cred);
}

#[test]
fn test_mark_unreachable() {
    let mut th = TestHarness::new();

    println!("spares = {:?}", th.ba.bridge_table.spares);
    println!("tmig = {:?}", th.ba.trustup_migration_table.table);
    println!("bmig = {:?}", th.ba.blockage_migration_table.table);
    println!("openinv = {:?}\n", th.bdb.openinv_buckets);

    // Mark a bridge in an untrusted bucket as unreachable
    let b6 = th.ba.bridge_table.buckets[6][0];
    th.ba.bridge_unreachable(&b6, &mut th.bdb);

    println!("spares = {:?}", th.ba.bridge_table.spares);
    println!("tmig = {:?}", th.ba.trustup_migration_table.table);
    println!("bmig = {:?}", th.ba.blockage_migration_table.table);
    println!("openinv = {:?}\n", th.bdb.openinv_buckets);

    // Mark another bridge grouped to the same trusted bucket as
    // unreachable
    let b7 = th.ba.bridge_table.buckets[7][0];
    th.ba.bridge_unreachable(&b7, &mut th.bdb);

    println!("spares = {:?}", th.ba.bridge_table.spares);
    println!("tmig = {:?}", th.ba.trustup_migration_table.table);
    println!("bmig = {:?}", th.ba.blockage_migration_table.table);
    println!("openinv = {:?}\n", th.bdb.openinv_buckets);

    // That will have introduced a blockage migration.  Get the target
    let target: u32 = *th
        .ba
        .blockage_migration_table
        .table
        .iter()
        .next()
        .unwrap()
        .1;

    // Block two of the bridges in that target bucket
    let bt1 = th.ba.bridge_table.buckets[target as usize][1];
    let bt2 = th.ba.bridge_table.buckets[target as usize][2];
    th.ba.bridge_unreachable(&bt1, &mut th.bdb);
    th.ba.bridge_unreachable(&bt2, &mut th.bdb);

    println!("spares = {:?}", th.ba.bridge_table.spares);
    println!("tmig = {:?}", th.ba.trustup_migration_table.table);
    println!("bmig = {:?}", th.ba.blockage_migration_table.table);
    println!("openinv = {:?}\n", th.bdb.openinv_buckets);
}

#[test]
fn test_blockage_migration() {
    let mut th = TestHarness::new();

    // Join an untrusted user
    let cred = th.open_invite().1 .0;

    // Time passes
    th.advance_days(47);

    // Go up to level 1
    let (_mperf_stat, migcred) = th.trust_promotion(&cred);
    let (_perf_stat, cred1) = th.level0_migration(&cred, &migcred);
    assert!(scalar_u32(&cred1.trust_level).unwrap() == 1);

    // Time passes
    th.advance_days(20);

    // Go up to level 2
    let (_two_perf_stat, cred2) = th.level_up(&cred1);
    assert!(scalar_u32(&cred2.trust_level).unwrap() == 2);
    println!("cred2 = {:?}", cred2);
    assert!(th.ba.verify_lox(&cred2));

    // Time passes
    th.advance_days(29);

    // Go up to level 3
    let (_three_perf_stat, cred3) = th.level_up(&cred2);
    assert!(scalar_u32(&cred3.trust_level).unwrap() == 3);
    println!("cred3 = {:?}", cred3);
    assert!(th.ba.verify_lox(&cred3));

    // Get our bridges
    let (id, key) = bridge_table::from_scalar(cred3.bucket).unwrap();
    let encbuckets = th.ba.enc_bridge_table();
    let bucket =
        bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();
    // We should have a Bridge Reachability credential
    assert!(bucket.1.is_some());

    // Oh, no!  Two of our bridges are blocked!
    th.ba.bridge_unreachable(&bucket.0[0], &mut th.bdb);
    th.ba.bridge_unreachable(&bucket.0[2], &mut th.bdb);

    println!("spares = {:?}", th.ba.bridge_table.spares);
    println!("tmig = {:?}", th.ba.trustup_migration_table.table);
    println!("bmig = {:?}", th.ba.blockage_migration_table.table);
    println!("openinv = {:?}\n", th.bdb.openinv_buckets);

    // Time passes
    th.advance_days(1);

    let encbuckets2 = th.ba.enc_bridge_table();
    let bucket2 =
        bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets2[id as usize]).unwrap();
    // We should no longer have a Bridge Reachability credential
    assert!(bucket2.1.is_none());

    // See about getting a Migration credential for the blockage
    let (_block_perf_stat, migration) = th.check_blockage(&cred3);

    println!("migration = {:?}", migration);

    // Migrate
    let (_four_perf_stat, cred4) = th.blockage_migration(&cred3, &migration);

    println!("cred4 = {:?}", cred4);
    assert!(th.ba.verify_lox(&cred4));
}

#[test]
fn stats_test_trust_levels() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);

        let mut promo_req_size: Vec<f64> = Vec::new();
        let mut promo_resp_size: Vec<f64> = Vec::new();
        let mut promo_req_time: Vec<f64> = Vec::new();
        let mut promo_resp_time: Vec<f64> = Vec::new();
        let mut promo_resp_handle_time: Vec<f64> = Vec::new();
        let mut mig_req_size: Vec<f64> = Vec::new();
        let mut mig_resp_size: Vec<f64> = Vec::new();
        let mut mig_req_time: Vec<f64> = Vec::new();
        let mut mig_resp_time: Vec<f64> = Vec::new();
        let mut mig_resp_handle_time: Vec<f64> = Vec::new();
        let mut sec_req_size: Vec<f64> = Vec::new();
        let mut sec_resp_size: Vec<f64> = Vec::new();
        let mut sec_req_time: Vec<f64> = Vec::new();
        let mut sec_resp_time: Vec<f64> = Vec::new();
        let mut sec_resp_handle_time: Vec<f64> = Vec::new();
        let mut three_req_size: Vec<f64> = Vec::new();
        let mut three_resp_size: Vec<f64> = Vec::new();
        let mut three_req_time: Vec<f64> = Vec::new();
        let mut three_resp_time: Vec<f64> = Vec::new();
        let mut three_resp_handle_time: Vec<f64> = Vec::new();
        let mut four_req_size: Vec<f64> = Vec::new();
        let mut four_resp_size: Vec<f64> = Vec::new();
        let mut four_req_time: Vec<f64> = Vec::new();
        let mut four_resp_time: Vec<f64> = Vec::new();
        let mut four_resp_handle_time: Vec<f64> = Vec::new();
        let mut open_req_size: Vec<f64> = Vec::new();
        let mut open_resp_size: Vec<f64> = Vec::new();
        let mut open_req_time: Vec<f64> = Vec::new();
        let mut open_resp_time: Vec<f64> = Vec::new();
        let mut open_resp_handle_time: Vec<f64> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let (open_perf_stat, cred) = th.open_invite();
            th.advance_days(30);
            let (tp_perf_stat, migcred) = th.trust_promotion(&cred.0);
            let (mig_perf_stat, cred1) = th.level0_migration(&cred.0, &migcred);
            th.advance_days(14);
            let (sec_perf_stat, cred2) = th.level_up(&cred1);
            th.advance_days(28);
            let (three_perf_stat, cred3) = th.level_up(&cred2);
            th.advance_days(56);
            let (four_perf_stat, _) = th.level_up(&cred3);
            open_req_size.push(open_perf_stat.req_len as f64);
            open_req_time.push(open_perf_stat.req_t.as_secs_f64());
            open_resp_size.push(open_perf_stat.resp_len as f64);
            open_resp_time.push(open_perf_stat.resp_t.as_secs_f64());
            open_resp_handle_time.push(open_perf_stat.resp_handle_t.as_secs_f64());
            promo_req_size.push(tp_perf_stat.req_len as f64);
            promo_req_time.push(tp_perf_stat.req_t.as_secs_f64());
            promo_resp_size.push(tp_perf_stat.resp_len as f64);
            promo_resp_time.push(tp_perf_stat.resp_t.as_secs_f64());
            promo_resp_handle_time.push(tp_perf_stat.resp_handle_t.as_secs_f64());
            mig_req_size.push(mig_perf_stat.req_len as f64);
            mig_req_time.push(mig_perf_stat.req_t.as_secs_f64());
            mig_resp_size.push(mig_perf_stat.resp_len as f64);
            mig_resp_time.push(mig_perf_stat.resp_t.as_secs_f64());
            mig_resp_handle_time.push(mig_perf_stat.resp_handle_t.as_secs_f64());
            sec_req_size.push(sec_perf_stat.req_len as f64);
            sec_req_time.push(sec_perf_stat.req_t.as_secs_f64());
            sec_resp_size.push(sec_perf_stat.resp_len as f64);
            sec_resp_time.push(sec_perf_stat.resp_t.as_secs_f64());
            sec_resp_handle_time.push(sec_perf_stat.resp_handle_t.as_secs_f64());
            three_req_size.push(three_perf_stat.req_len as f64);
            three_req_time.push(three_perf_stat.req_t.as_secs_f64());
            three_resp_size.push(three_perf_stat.resp_len as f64);
            three_resp_time.push(three_perf_stat.resp_t.as_secs_f64());
            three_resp_handle_time.push(three_perf_stat.resp_handle_t.as_secs_f64());
            four_req_size.push(four_perf_stat.req_len as f64);
            four_req_time.push(four_perf_stat.req_t.as_secs_f64());
            four_resp_size.push(four_perf_stat.resp_len as f64);
            four_resp_time.push(four_perf_stat.resp_t.as_secs_f64());
            four_resp_handle_time.push(four_perf_stat.resp_handle_t.as_secs_f64());
        }

        println!("\n***START: {}*3*2 BUCKETS LEVELS***\n", x);
        println!("\n----OPEN-INVITATION-{}---\n", x);
        print_stats_test_results(
            open_req_size,
            open_req_time,
            open_resp_size,
            open_resp_time,
            open_resp_handle_time,
        );

        println!("\n----TRUST-PROMOTION-1: 30 days-{}---\n", x);
        print_stats_test_results(
            promo_req_size,
            promo_req_time,
            promo_resp_size,
            promo_resp_time,
            promo_resp_handle_time,
        );

        println!("\n----TRUST-MIGRATION-0: 30 days-{}---\n", x);
        print_stats_test_results(
            mig_req_size,
            mig_req_time,
            mig_resp_size,
            mig_resp_time,
            mig_resp_handle_time,
        );

        println!("\n----LEVEL-UP-2: 44 days-{}---\n", x);
        print_stats_test_results(
            sec_req_size,
            sec_req_time,
            sec_resp_size,
            sec_resp_time,
            sec_resp_handle_time,
        );

        println!("\n----LEVEL-UP-3: 72 days---{}---\n", x);
        print_stats_test_results(
            three_req_size,
            three_req_time,
            three_resp_size,
            three_resp_time,
            three_resp_handle_time,
        );

        println!("\n----LEVEL-UP-4: 128 days---{}---\n", x);
        print_stats_test_results(
            four_req_size,
            four_req_time,
            four_resp_size,
            four_resp_time,
            four_resp_handle_time,
        );
    }
}

#[test]
fn stats_test_invitations() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);

        let mut req_size: Vec<f64> = Vec::new();
        let mut resp_size: Vec<f64> = Vec::new();
        let mut req_time: Vec<f64> = Vec::new();
        let mut resp_time: Vec<f64> = Vec::new();
        let mut resp_handle_time: Vec<f64> = Vec::new();
        let mut red_req_size: Vec<f64> = Vec::new();
        let mut red_resp_size: Vec<f64> = Vec::new();
        let mut red_req_time: Vec<f64> = Vec::new();
        let mut red_resp_time: Vec<f64> = Vec::new();
        let mut red_resp_handle_time: Vec<f64> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());

            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            th.advance_days(28);
            let (perf_stat, (_, invite)) = th.issue_invite(&cred2);
            let (bob_perf_stat, _) = th.redeem_invite(&invite);
            req_size.push(perf_stat.req_len as f64);
            req_time.push(perf_stat.req_t.as_secs_f64());
            resp_size.push(perf_stat.resp_len as f64);
            resp_time.push(perf_stat.resp_t.as_secs_f64());
            resp_handle_time.push(perf_stat.resp_handle_t.as_secs_f64());
            red_req_size.push(bob_perf_stat.req_len as f64);
            red_req_time.push(bob_perf_stat.req_t.as_secs_f64());
            red_resp_size.push(bob_perf_stat.resp_len as f64);
            red_resp_time.push(bob_perf_stat.resp_t.as_secs_f64());
            red_resp_handle_time.push(bob_perf_stat.resp_handle_t.as_secs_f64());
        }

        println!("\n***START: {}*3*2 BUCKETS INVITATIONS***\n", x);
        println!("\n----ISSUE-INVITATION-{}---\n", x);
        print_stats_test_results(req_size, req_time, resp_size, resp_time, resp_handle_time);

        println!("\n----REDEEM-INVITATION-{}---\n", x);
        print_stats_test_results(
            red_req_size,
            red_req_time,
            red_resp_size,
            red_resp_time,
            red_resp_handle_time,
        );
    }
}

#[test]
fn stats_test_percent_blockage_migration_05() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 5***\n", x);
        block_bridges(&mut th, 5, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_010() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 10***\n", x);
        block_bridges(&mut th, 10, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_15() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 15***\n", x);
        block_bridges(&mut th, 15, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_20() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 20***\n", x);
        block_bridges(&mut th, 20, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_25() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 25***\n", x);
        block_bridges(&mut th, 25, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_30() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 30***\n", x);
        block_bridges(&mut th, 30, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_35() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 35***\n", x);
        block_bridges(&mut th, 35, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_40() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 40***\n", x);
        block_bridges(&mut th, 40, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_45() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 45***\n", x);
        block_bridges(&mut th, 45, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_50() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 50***\n", x);
        block_bridges(&mut th, 50, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_55() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 55***\n", x);
        block_bridges(&mut th, 55, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_60() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 60***\n", x);
        block_bridges(&mut th, 60, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_65() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 65***\n", x);
        block_bridges(&mut th, 65, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_70() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 70***\n", x);
        block_bridges(&mut th, 70, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_75() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 75***\n", x);
        block_bridges(&mut th, 75, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_80() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 80***\n", x);
        block_bridges(&mut th, 80, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_85() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 85***\n", x);
        block_bridges(&mut th, 85, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_90() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 90***\n", x);
        block_bridges(&mut th, 90, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_95() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }

        println!("\n***START: {}*3*2 BUCKETS 95***\n", x);
        block_bridges(&mut th, 95, credentials);
    }
}

#[test]
fn stats_test_percent_blockage_migration_100() {
    let buckets: Vec<u16> = vec![150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500];

    for x in buckets {
        let mut th = TestHarness::new_buckets(x, x);
        let mut credentials: Vec<cred::Lox> = Vec::new();
        for _ in 0..USERS {
            let h: NaiveTime = DateTime::time(&Utc::now());
            if h.hour() == 23 && h.minute() == 59 {
                println!("Wait for UTC 00:00");
                thread::sleep(Duration::new(60, 0));
                println!("Ready to work again");
            }
            let cred = th.open_invite().1 .0;
            th.advance_days(30);
            let (_, migcred) = th.trust_promotion(&cred);
            let (_, cred1) = th.level0_migration(&cred, &migcred);
            th.advance_days(14);
            let (_, cred2) = th.level_up(&cred1);
            let (_, (cred2a, invite)) = th.issue_invite(&cred2);
            let (_, bob_cred) = th.redeem_invite(&invite);
            th.advance_days(28);
            let (_, _) = th.level_up(&bob_cred);
            let (_, cred3) = th.level_up(&cred2a);
            credentials.push(cred3);
        }
        println!("\n***START: {}*3*2 BUCKETS 100***\n", x);
        block_bridges(&mut th, 100, credentials);
    }
}

/// Blocks a percentage of the bridges for the passed Test Harness
/// excluding the hot spare buckets as they will not have been handed out.
/// The logic assumes hot spare buckets are appended to the end of the bridge_table
/// bucket list.

fn block_bridges(th: &mut TestHarness, percentage: usize, credentials: Vec<cred::Lox>) {
    let blockable_num = th.ba.bridge_table.buckets.len()
        - th.ba.bridge_table.spares.len()
        - th.bdb.openinv_buckets.len();
    let blockable_range = th.ba.bridge_table.buckets.len() - th.ba.bridge_table.spares.len();
    let to_block: usize = blockable_num * percentage / 100;
    let mut block_index: HashSet<usize> = HashSet::new();
    let mut rng = rand::thread_rng();

    while block_index.len() < to_block {
        let rand_num = rng.gen_range(0, blockable_range);
        if !th.bdb.openinv_buckets.contains(&(rand_num as u32)) {
            block_index.insert(rand_num);
        }
    }

    for index in block_index {
        let b0 = th.ba.bridge_table.buckets[index][0];
        let b1 = th.ba.bridge_table.buckets[index][1];
        let b2 = th.ba.bridge_table.buckets[index][2];
        th.ba.bridge_unreachable(&b0, &mut th.bdb);
        th.ba.bridge_unreachable(&b1, &mut th.bdb);
        th.ba.bridge_unreachable(&b2, &mut th.bdb);
    }

    let mut req_size: Vec<f64> = Vec::new();
    let mut resp_size: Vec<f64> = Vec::new();
    let mut req_time: Vec<f64> = Vec::new();
    let mut resp_time: Vec<f64> = Vec::new();
    let mut resp_handle_time: Vec<f64> = Vec::new();
    let mut red_req_size: Vec<f64> = Vec::new();
    let mut red_resp_size: Vec<f64> = Vec::new();
    let mut red_req_time: Vec<f64> = Vec::new();
    let mut red_resp_time: Vec<f64> = Vec::new();
    let mut red_resp_handle_time: Vec<f64> = Vec::new();
    for cred in credentials {
        let (id, key) = bridge_table::from_scalar(cred.bucket).unwrap();
        let encbuckets = th.ba.enc_bridge_table();
        let bucket =
            bridge_table::BridgeTable::decrypt_bucket(id, &key, &encbuckets[id as usize]).unwrap();

        let mut count = 0;
        for bridge_line in &bucket.0 {
            if th.ba.bridge_table.reachable.contains_key(bridge_line) {
                count += 1;
            }
        }

        if count < 2 {
            let (perf_stat, migration) = th.check_blockage(&cred);
            let (block_perf_stat, _) = th.blockage_migration(&cred, &migration);
            req_size.push(perf_stat.req_len as f64);
            req_time.push(perf_stat.req_t.as_secs_f64());
            resp_size.push(perf_stat.resp_len as f64);
            resp_time.push(perf_stat.resp_t.as_secs_f64());
            resp_handle_time.push(perf_stat.resp_handle_t.as_secs_f64());
            red_req_size.push(block_perf_stat.req_len as f64);
            red_req_time.push(block_perf_stat.req_t.as_secs_f64());
            red_resp_size.push(block_perf_stat.resp_len as f64);
            red_resp_time.push(block_perf_stat.resp_t.as_secs_f64());
            red_resp_handle_time.push(block_perf_stat.resp_handle_t.as_secs_f64());
        }
    }

    println!("\n----CHECK-BLOCKAGE-{}----\n", percentage);
    print_stats_test_results(req_size, req_time, resp_size, resp_time, resp_handle_time);

    println!("\n----BLOCKAGE-MIGRATION-{}----\n", percentage);
    print_stats_test_results(
        red_req_size,
        red_req_time,
        red_resp_size,
        red_resp_time,
        red_resp_handle_time,
    );
}

fn print_test_results(perf_stat: PerfStat) {
    println!("Request size = {:?} bytes", perf_stat.req_len);
    println!("Request time = {:?}", perf_stat.req_t);
    println!("Response size = {:?} bytes", perf_stat.resp_len);
    println!("Response time = {:?}", perf_stat.resp_t);
    println!("Response handle time = {:?}", perf_stat.resp_handle_t);
}

//fn print_time_test_results(perf_stat: PerfStat) {
//    println!("Request time = {:?}", perf_stat.req_t);
//    println!("Response time = {:?}", perf_stat.resp_t);
//    println!("Response handle time = {:?}", perf_stat.resp_handle_t);
//}

fn print_stats_test_results(
    req_size: Vec<f64>,
    req_time: Vec<f64>,
    resp_size: Vec<f64>,
    resp_time: Vec<f64>,
    resp_handle_time: Vec<f64>,
) {
    let mean_req_size = if req_size.len() > 0 {
        mean(&req_size)
    } else {
        0.0
    };
    let req_std_dev = if req_size.len() > 1 {
        standard_deviation(&req_size, Some(mean_req_size))
    } else {
        0.0
    };
    let mean_req_time = if req_time.len() > 0 {
        mean(&req_time)
    } else {
        0.0
    };
    let req_time_std_dev = if req_time.len() > 1 {
        standard_deviation(&req_time, Some(mean_req_time))
    } else {
        0.0
    };
    let mean_resp_size = if resp_size.len() > 0 {
        mean(&resp_size)
    } else {
        0.0
    };
    let resp_std_dev = if resp_size.len() > 1 {
        standard_deviation(&resp_size, Some(mean_resp_size))
    } else {
        0.0
    };
    let mean_resp_time = if resp_time.len() > 0 {
        mean(&resp_time)
    } else {
        0.0
    };
    let resp_time_std_dev = if resp_time.len() > 1 {
        standard_deviation(&resp_time, Some(mean_resp_time))
    } else {
        0.0
    };
    let mean_resp_handle_time = if resp_handle_time.len() > 0 {
        mean(&resp_handle_time)
    } else {
        0.0
    };
    let resp_handle_time_std_dev = if resp_handle_time.len() > 1 {
        standard_deviation(&resp_handle_time, Some(mean_resp_handle_time))
    } else {
        0.0
    };

    println!("Average request size = {} bytes", mean_req_size);
    println!("Request size standard deviation = {} bytes", req_std_dev);
    println!(
        "Average request time = {:?}",
        Duration::from_secs_f64(mean_req_time)
    );
    println!(
        "Request time standard deviation = {:?}",
        Duration::from_secs_f64(req_time_std_dev)
    );
    println!("Average response size = {} bytes", mean_resp_size);
    println!("Response standard deviation = {} bytes", resp_std_dev);
    println!(
        "Average response time = {:?}",
        Duration::from_secs_f64(mean_resp_time)
    );
    println!(
        "Response time standard deviation = {:?}",
        Duration::from_secs_f64(resp_time_std_dev)
    );
    println!(
        "Average response handling time = {:?}",
        Duration::from_secs_f64(mean_resp_handle_time)
    );
    println!(
        "Response handling time standard deviation = {:?}",
        Duration::from_secs_f64(resp_handle_time_std_dev)
    );
}
