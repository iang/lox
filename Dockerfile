FROM rust:1.56.0
WORKDIR /home/lox
ADD src/ ./src/
ADD Cargo.toml Cargo.toml
ADD tests/ ./tests/
ADD run_tests_fast.sh .
ADD Parsing-results ./Parsing-results
ADD README.md README.md
RUN apt update -y
RUN apt install -y python3 python3-pip
RUN pip3 install -r Parsing-results/requirements.txt
RUN cargo build --release
ENV SHELL=/bin/bash
