import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

fig, (axs, axs1) = plt.subplots(2, 4, figsize=(24, 12))
fig.suptitle("Checking for Blockage and Migration to new Bridges for" +
             " Percentage of Bridges Blocked")
columns = ["Percent", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
           "ReTstdev", "ResponseHT", "RHTstdev"]
df = pd.read_csv("check_blockage.csv", usecols=columns)
df1 = pd.read_csv("migration.csv", usecols=columns)
fig.supxlabel('Blocked Bridges (%)')
axs[0].set_ylabel('Request Time (ms)')
axs[0].set_ylim([0,70])
l1 = axs[0].plot(df.Percent, df.RequestT, color='#CC4F1B',
            label='Request Time for Percentage of Bridges Blocked')
axs[0].fill_between(df.Percent, df.RequestT-df.Rtstdev, df.RequestT+df.Rtstdev,
                    alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
axs1[0].set_ylabel('Request Time (ms)')
axs1[0].set_ylim([0,70])
axs1[0].plot(df1.Percent, df1.RequestT, color='#1B2ACC',
             label='Request Time for Percentage of Bridges Blocked')
axs1[0].fill_between(df1.Percent, df1.RequestT-df1.Rtstdev,
                    df1.RequestT+df1.Rtstdev, alpha=0.2, edgecolor='#1B2ACC',
                    facecolor='#089FFF')
axs[1].set_ylabel('Response Time (ms)')
axs[1].plot(df.Percent, df.ResponseT, color='#CC4F1B',
             label='Response Time for Percentage of Bridges Blocked')
axs[1].fill_between(df.Percent, df.ResponseT-df.ReTstdev,
                     df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#CC4F1B',
                     facecolor='#FF9848')
axs1[1].set_ylabel('Response Time (ms)')
axs1[1].set_ylim([0,70])
axs1[1].plot(df1.Percent, df1.ResponseT, color='#1B2ACC',
             label='Response Time for Percentage of Bridges Blocked')
axs1[1].fill_between(df1.Percent, df1.ResponseT-df1.ReTstdev,
                     df1.ResponseT+df1.ReTstdev, alpha=0.2, edgecolor='#1B2ACC',
                     facecolor='#089FFF')
axs[2].set_ylabel('Response Size (ms)')
axs[2].plot(df.Percent, df.ResponseS, color='#CC4F1B',
             label='Response Size for Percentage of Bridges Blocked')
axs1[2].set_ylabel('Response Size (ms)')
axs1[2].plot(df1.Percent, df1.ResponseS, color='#1B2ACC',
             label='Response Size for Percentage of Bridges Blocked')
axs[3].set_ylabel('Response Handling Time (ms)')
axs[3].plot(df.Percent, df.ResponseHT, color='#CC4F1B',
             label='Response Handling Time for Percentage of Bridges Blocked')
axs[3].fill_between(df.Percent, df.ResponseHT-df.RHTstdev,
                     df.ResponseHT+df.RHTstdev, alpha=0.5, edgecolor='#CC4F1B',
                     facecolor='#FF9848')
axs1[3].set_ylabel('Response Handling Time (ms)')
axs1[3].set_ylim([0,70])
axs1[3].plot(df1.Percent, df1.ResponseHT, color='#1B2ACC',
             label='Response Handling Time for Percentage of Bridges Blocked')
axs1[3].fill_between(df1.Percent, df1.ResponseHT-df1.RHTstdev,
                     df1.ResponseHT+df1.RHTstdev, alpha=0.2, edgecolor='#1B2ACC',
                     facecolor='#089FFF')
legend_elements = [Line2D([0], [0], color='#CC4F1B', label="Check Blockage Protocol"), Line2D([0], [0], color='#1B2ACC', label="Blockage Migration Protocol")]
fig.legend(handles=legend_elements, loc='lower right')
fig.savefig("Performance.pdf")
