import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def main():

    fig, axs = plt.subplots(1, 3, figsize=(24, 7))
    columns = ["Percent","Bridges", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
               "ReTstdev", "ResponseHT", "RHTstdev"]
    df = pd.read_csv("standard_check"+".csv", usecols=columns)
    df.sort_values(["Percent"], axis=0,ascending=[False], inplace=True)
    bridges = df.Bridges*2*3
    fig.supxlabel('Blocked Bridges (%)', size=30)
    axs[0].set_ylabel('Response Time (ms)', size=25)
    axs[0].tick_params(axis='x', labelsize=15)
    axs[0].tick_params(axis='y', labelsize=15)
#    axs[0].set_xticklabels('Blocked Bridges (%)',fontsize=10)
    axs[0].plot(df.Percent, df.ResponseT, color='#CC4F1B',
                label='Response Time for Percentage of Bridges Blocked')
    axs[0].fill_between(df.Percent, df.ResponseT-df.ReTstdev,
                df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#CC4F1B',
                facecolor='#FF9848')
    axs[1].set_ylabel('Response Size (bytes)', size=25)
    axs[1].tick_params(axis='x', labelsize=15)
    axs[1].tick_params(axis='y', labelsize=15)
    axs[1].plot(df.Percent, df.ResponseS, color='#CC4F1B',
                label='Response Size for Percentage of Bridges Blocked')
    axs[2].set_ylabel('Response Handling Time (ms)', size=25)
    axs[2].tick_params(axis='x', labelsize=15)
    axs[2].tick_params(axis='y', labelsize=15)
    axs[2].plot(df.Percent, df.ResponseHT, color='#CC4F1B',
                label='Response Handling Time for Percentage of Bridges Blocked')
    axs[2].fill_between(df.Percent, df.ResponseHT-df.RHTstdev,
                df.ResponseHT+df.RHTstdev, alpha=0.5, edgecolor='#CC4F1B',
                facecolor='#FF9848')
    fig. tight_layout(pad=1)
    fig.savefig("StandardCheck.pdf")
    plt.close('all')

    for n in range(5,105,5):
        fig, axs = plt.subplots(1, 3, figsize=(24, 7))
        columns = ["Percent","Bridges", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
               "ReTstdev", "ResponseHT", "RHTstdev"]
        df = pd.read_csv("checkblockage"+str(n)+".csv", usecols=columns)
        bridges = df.Bridges*2*3
        fig.supxlabel('Total Size of Bridge Pool ('+str(n)+'% Bridges Blocked)',size=30)
        axs[0].set_ylabel('Response Time (ms)', size=25)
        axs[0].tick_params(axis='x', labelsize=15)
        axs[0].tick_params(axis='y', labelsize=15)
        axs[0].plot(bridges, df.ResponseT, color='#740202',
                    label='Response Time for Percentage of Bridges Blocked')
        axs[0].fill_between(bridges, df.ResponseT-df.ReTstdev,
                            df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#740202',
                            facecolor='#E75252')
        axs[1].set_ylabel('Response Size (bytes)', size=25)
        axs[1].tick_params(axis='x', labelsize=15)
        axs[1].tick_params(axis='y', labelsize=15)
        axs[1].plot(bridges, df.ResponseS, color='#740202',
                    label='Response Size for Percentage of Bridges Blocked')
        axs[2].set_ylabel('Response Handling Time (ms)', size=25)
        axs[2].tick_params(axis='x', labelsize=15)
        axs[2].tick_params(axis='y', labelsize=15)
        axs[2].plot(bridges, df.ResponseHT, color='#740202',
                    label='Response Handling Time for Percentage of Bridges Blocked')
        axs[2].fill_between(bridges, df.ResponseHT-df.RHTstdev,
                            df.ResponseHT+df.RHTstdev, alpha=0.5, edgecolor='#740202',
                            facecolor='#E75252')
        fig. tight_layout(pad=1)
        fig.savefig("PerformanceVaried"+str(n)+".pdf")
        print("\nDone PerformanceVaried"+str(n)+" Plot.\nOutput to: PerformanceVaried"+str(n)+".pdf")
        plt.close('all')


def set_plot_options():
      options = {
          'font.size': 12,
          'figure.figsize': (4,2),
          'figure.dpi': 100.0,
          'figure.subplot.left': 0.20,
          'figure.subplot.right': 0.97,
          'figure.subplot.bottom': 0.20,
          'figure.subplot.top': 0.90,
          'grid.color': '0.1',
          'grid.linestyle': ':',
          #'grid.linewidth': 0.5,
          'axes.grid' : True,
          #'axes.grid.axis' : 'y',
          #'axes.axisbelow': True,
          'axes.titlesize' : 25,
          'axes.labelsize' : 25,
          'axes.formatter.limits': (-4,4),
          'xtick.labelsize' : 30,#get_tick_font_size_10(),
          'ytick.labelsize' : 30,#get_tick_font_size_10(),
          'lines.linewidth' : 2.0,
          'lines.markeredgewidth' : 0.5,
          'lines.markersize' : 15,
      }

      for option_key in options:
          matplotlib.rcParams[option_key] = options[option_key]
      if 'figure.max_num_figures' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_num_figures'] = 100
      if 'figure.max_open_warning' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_open_warning'] = 100

if __name__ == "__main__":
    sys.exit(main())
