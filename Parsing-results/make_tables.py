import csv
import sys
import pandas as pd
import numpy as np


def main():
    perf = open("performance_stats"+".csv", "w", newline='')
    protocols=["Open Invitation", "Trust Promotion(0->1)",
               "Trust Migration (0->1)", "Level Up (1->4)", "Issue Invitation",
                    "Redeem Invitation", "Check Blockage 5%", "Check Blockage 50%", "Check Blockage 100%", "Blockage Migration"]
    files = ["trust_levels.csv", "trust_promo.csv", "trust_mig.csv", "level2.csv",
             "invitations.csv", "redeem_invites.csv","check_blockage5.csv",
             "check_blockage50.csv","check_blockage100.csv","blockage_migration50.csv"]
    csv_cols = ["RequestS", "RequestT","Rtstdev","ResponseS","ResponseT",
                "ReTstdev", "ResponseHT", "RHTstdev"]
    perf_columns = ["Protocol","Request Size", "Request Time", "sigma",
                    "Response Size","Response Time", "sigma",
                    "Response Handling Time", "sigma"]
    worst_resp = 0
    perfwriter = csv.writer(perf, delimiter=',')
    perfwriter.writerow(perf_columns)

    for i, protocol in enumerate(protocols):
        columns = ["Percent","Bridges", "RequestS", "Rsstdev", "RequestT",
                   "Rtstdev", "ResponseS","Restdev","ResponseT",
                   "ReTstdev", "ResponseHT", "RHTstdev"]
        df = pd.read_csv(files[i], usecols=columns)
        perf_in = []

        perf_in.append(protocol)
        for item in csv_cols:
            row = df[item].loc[df['Bridges']==900].values
            if "stdev" in item:
                rounded = np.round(row[0], decimals=1)
            else:
                rounded = np.round(row[0], decimals=3)
            perf_in.append(rounded)
            rounded = np.round(row[0], decimals=1)
            if item == "RequestT":
                req = np.round(rounded, decimals=1)
            elif item == "ResponseT":
                resp_sec = np.round(1000/rounded, decimals=1)
                resp_core = resp_sec/(1/(60*60*24))
                if rounded > worst_resp:
                    worst_resp = rounded


        perfwriter.writerow(perf_in)

    for i, protocol in enumerate(protocols):
        columns = ["Percent","Bridges", "RequestS", "Rsstdev", "RequestT",
                   "Rtstdev", "ResponseS","Restdev","ResponseT",
                   "ReTstdev", "ResponseHT", "RHTstdev"]
        df = pd.read_csv(files[i], usecols=columns)
        row = df['ResponseT'].loc[df['Bridges']==900].values
        rounded = np.round(row[0], decimals=3)
        resp_sec = np.round(1000/rounded, decimals=3)
        resp_core = int(resp_sec/(1/(60*60*24)))
        if worst_resp > rounded:
            secs = int(worst_resp/1000)

    perf.close()
    print("\nDone Tables.\nTable data output to: performance_stats.csv,\n")


if __name__ == "__main__":
    sys.exit(main())
