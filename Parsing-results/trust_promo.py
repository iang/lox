log_file = open("trust_levels.log", "r").readlines()
trust_promo_file = open("trust_promo"+".csv", "w")
trust_promo_file.write("Bridges,RequestS,Rsstdev,RequestT,Rtstdev,ResponseS,Restdev,ResponseT,ReTstdev,ResponseHT,RHTstdev\n")


bridges = 0
promo = 0
req_size = 0
req_size_std = 0
req_time = 0
req_time_std = 0
resp_size = 0
resp_size_std = 0
resp_time = 0
resp_time_std = 0
resp_handle_time = 0
resp_handle_std = 0
endline = 0

# Loop over the remaining lines in the file
for line in log_file:
    if "***START" in line:
        bridges = line.split()[1].split("*")[0]
    elif "TRUST-PROMOTION" in line:
        promo = 1
    elif promo:
        value = line.split(" = ")
        if value[0].startswith("Average"):
            if "request" in value[0]:
                if "size" in value[0]:
                    raw_size = value[1].split(" ")
                    req_size = raw_size[0]
                else:
                    raw_size = value[1]
                    req_time = raw_size[:-3]
            else:
                if "size" in value[0]:
                    raw_size = value[1].split(" ")
                    resp_size = raw_size[0]
                else:
                    raw_size = value[1]
                    if "handling" in value[0]:
                        resp_handle_time = raw_size[:-3]
                    elif (value[1][-3]) != "m":
                        sec = value[1][:-3]
                        resp_time = float(sec)*1000
                    else:
                        resp_time = raw_size[:-3]
        elif value[0].startswith("Request"):
            if "size" in value[0]:
                raw_size = value[1].split(" ")
                req_size_std = raw_size[0]
            else:
                if "µ" in value[1]:
                    micro_sec = value[1].split("µ")
                    to_sec = float(micro_sec[0])*0.001
                else:
                    to_sec = value[1][:-3]
                    req_time_std = to_sec

        elif value[0].startswith("Response"):
            if "size" in value[0]:
                raw_size = value[1].split(" ")
                resp_size_std = raw_size[0]
            elif "bytes" in value[1]:
                continue
            else:
                if "µ" in value[1]:
                    micro_sec = value[1].split("µ")
                    to_sec = float(micro_sec[0])*0.001
                else:
                    to_sec = value[1][:-3]
                if "handling" in value[0]:
                    resp_handle_time_std = to_sec
                    endline = 1
                else:
                    resp_time_std = to_sec

        if endline == 1:
            if promo == 1:
                trust_promo_file.write(str(bridges)+"," + str(req_size) + "," + str(req_size_std) + ","  + str(req_time) + "," + str(req_time_std) + ","  + str(resp_size) + "," + str(resp_size_std) + "," + str(resp_time) + "," + str(resp_time_std) + "," + str(resp_handle_time) + "," + str(resp_handle_time_std) + "\n")
                promo = 0
                endline = 0

trust_promo_file.close()
print("Done.")
