#!/usr/bin/env python

import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def main():
    set_plot_options()
    fig, axs = plt.subplots(1)
    columns = ["Percent","Bridges", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
               "ReTstdev", "ResponseHT", "RHTstdev"]
    df = pd.read_csv("standard_check"+".csv", usecols=columns)
    df.sort_values(["Percent"], axis=0,ascending=[False], inplace=True)
    bridges = df.Bridges*2*3
    axs.set_xlabel('Blocked Bridges (%)')
    axs.set_ylabel('Response Time (ms)')
    axs.plot(df.Percent, df.ResponseT, color='#CC4F1B',
                label='Response Time for Percentage of Bridges Blocked')
    axs.set_ylim(bottom=0)
    axs.fill_between(df.Percent, df.ResponseT-df.ReTstdev,
                df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#CC4F1B',
                facecolor='#FF9848')
    fig. tight_layout(pad=1)
    fig.savefig("StandardCheck.pdf")

def set_plot_options():
    options = {
        #'backend': 'PDF',
        'font.size': 18,
        'figure.figsize': (7,3.5),
        'figure.dpi': 100.0,
        'axes.grid' : True,
        'axes.xmargin' : 0,
        'axes.grid.axis' : 'y',
        'axes.axisbelow': True,
        'axes.titlesize' : 'medium',
        'axes.labelsize' : 'large',
        'axes.formatter.limits': (-6,6),
        'xtick.labelsize' : 18,#get_tick_font_size_10(),
        'ytick.labelsize' : 18,
        'lines.linewidth' : 2.0,
        'lines.markersize' : 10,
        # turn on the following to embedd fonts; requires latex
        'ps.useafm' : True,
        'pdf.use14corefonts' : True,
        'text.usetex' : False,
    }

    for option_key in options:
        matplotlib.rcParams[option_key] = options[option_key]

    if 'figure.max_num_figures' in matplotlib.rcParams:
        matplotlib.rcParams['figure.max_num_figures'] = 100
    if 'figure.max_open_warning' in matplotlib.rcParams:
        matplotlib.rcParams['figure.max_open_warning'] = 100
    if 'legend.ncol' in matplotlib.rcParams:
        matplotlib.rcParams['legend.ncol'] = 100

if __name__ == "__main__":
    sys.exit(main())
